<?php

use Illuminate\Database\Seeder;

class PhraseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      App\Phrase::create([
          'id'              => 1,
          'author'          => "JORGE CAMARGO | BE ORIGINAL",
          'description'     => "Phasellus luctus commodo ullamcorper a posuere rhoncus commodo elit. Aenean congue,risus utaliquam dapibus. Thanks!"
      ]);
    }
}
