<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'id'              => 2,
            'name'            => 'Agencia Creo',
            'email'           => 'admin@agenciacreo.com',
            'password'        => bcrypt('ACr30!'),
            'remember_token'  => str_random(10),
        ]);
    }
}
