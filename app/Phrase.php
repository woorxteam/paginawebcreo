<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Phrase extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'phrase';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public static function phrase()
    {
        $publication = \DB::table('phrase')
                        ->select('title', 'id', 'description')
                        ->first();

        return $publication;
    }



}
