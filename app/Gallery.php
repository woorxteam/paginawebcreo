<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'gallery';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public static function galleries()
    {
        $galleries = \DB::table('gallery')
                        ->select('id', 'title', 'file_small', 'file_big', 'file_big2', 'file_big3', 'description')
                        ->get();

        return $galleries;
    }

}
