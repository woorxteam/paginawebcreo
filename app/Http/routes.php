<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::resource('/', 'PageController');
/*Route::resource('/', 'PageController@construction');*/

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', ['as' =>'auth/login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('auth/logout', ['as' => 'auth/logout', 'uses' => 'Auth\AuthController@getLogout']);

Route::group(['middleware'  => 'auth'], function(){
    Route::resource('/gallery', 'GalleryController');
    Route::resource('/phrase', 'PhraseController');

    Route::post('/image/upload/publication','PublicationController@upload');
    Route::put('/image/upload/publication','PublicationController@upload');
    Route::post('/image/upload','GalleryController@upload');
});

Route::pattern('inexistentes', '.*');
Route::any('/{inexistentes}', function()
{
	 return view('error');
});
