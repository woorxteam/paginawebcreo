<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Publication;
use App\Gallery;
use App\Phrase;
use Validator;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galleries = Gallery::galleries();
        $phrases = Phrase::take(3)->get();
        $data = array(
            'galleries'   => $galleries,
            'phrases'      => $phrases
        );

        return view('page.index', $data);
    }

    public function send(Request $request)
    {
        $messages = [
            'name.required'       => 'El campo nombre es obligatorio.',
            'email.required'      => 'El campo email es obligatorio.',
            'email.email'         => 'El campo email no es válido.',
        ];

        $validator = Validator::make($request->all(), [
            'name'               => 'required',
            'email'              => 'required|email'
        ], $messages);

        if ($validator->fails()) {
            return redirect('/contacto')
                        ->withErrors($validator)
                        ->withInput();
        }

        $contact  = $request->all();

        try {

          //---->Enviamos la cotización al contacto
          \Mail::send('emails.contact', $contact, function($message) use ($contact)
          {
               //remitente
               $message->from("alejandro.delpiero@gmail.com", "Tours Ejecutivos");

               //asunto
               $message->subject("Contacto desde el sitio web Tours ejecutivos");

               //receptor
               $message->to("alex@woorx.mx", "Tours Ejecutivos");
          });
        } catch (\ErrorException $e) {
            dd($e);
            \Session::flash('add_errors','error');
            return redirect("/")->with('add_errors', true);
        }

        return redirect("/");

    }
    public function construction()
    {

        return view('page.construction');
    }

}
