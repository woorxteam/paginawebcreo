<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gallery;
use Validator;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galleries = Gallery::galleries();
        $data = array(
            'galleries'   => $galleries
        );

        return view('gallery.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('gallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'title.required'              => 'El campo título es obligatorio.',
            'image_small.required'        => 'La imagen pequeña es obligatoria.',
            'image_big.required'          => 'La imagen de mayor tamaño es obligatoria.'
        ];

        $validator = Validator::make($request->all(), [
            'title'              => 'required',
            'image_small'        => 'required'
        ], $messages);

        if ($validator->fails()) {
            return redirect('gallery/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        $gallery  = $request->all();

        try {
            \DB::transaction(function() use ($gallery) {

                $row = new Gallery;
                $row->title            = $gallery['title'];
                $row->file_small       = $gallery['image_small'];
                $row->file_big         = $gallery['image_big'];
                $row->file_big2        = ($gallery['image_big2'])?$gallery['image_big2']:null;
                $row->file_big3        = ($gallery['image_big3'])?$gallery['image_big3']:null;
                $row->description      = $gallery['description'];
                $row->save();

            });

        } catch (\ErrorException $e) {
            \Session::flash('add_errors','error');
            return redirect('/gallery/create')->with('add_errors', true);
        }

        return redirect("/gallery")->with('status', 'Galería creada con éxito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
          $row = Gallery::find($id);
          Gallery::destroy($id);

          \File::Delete(public_path().'/images/' . $row->file_small);
          \File::Delete(public_path().'/images/' . $row->file_big);

        } catch (\ErrorException $e) {
            \Session::flash('add_errors','error');
            return redirect('/gallery')->with('delete_errors', true);
        }

        return redirect('/gallery')->with('status', 'Galería eliminada con éxito!');
    }


    public function upload(Request $request)
    {
        $messages = [
          'documentation.required'         => 'La documentación es obligatoria.',
          'documentation.mimes'            => 'Solo se permiten archivos tipo JPG.'
        ];

        $validator = Validator::make($request->all(), [
            'documentation'      => 'required|mimes:jpeg,png'
        ], $messages);

        if ($validator->fails()) {
            return response()->json(array('exito' => false), 200);
        } else {
            $document = $request->all();

            $file = $request->file('documentation');
            $documentation = "";

            if ($file != null) {
                $size = $file->getClientSize();
                $extension = $file->getClientOriginalExtension();
                $documentation = md5(time()) . "." . $extension;
                $maximo = 2;

                if ($size > 0) {
                    if (($size / 1000000) > $maximo){
                        return response()->json(array('exito' => false), 200);
                    }
                } else {
                    return response()->json(array('exito' => false), 200);
                }

                \Storage::disk('image')->put($documentation,  \File::get($file));
                return response()->json(array('exito' => true, 'file' => $documentation), 200);
            }
        }

        return response()->json(array('exito' => false), 200);
    }
}
