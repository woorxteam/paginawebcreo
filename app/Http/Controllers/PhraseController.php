<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Phrase;
use Validator;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PhraseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $publication = Phrase::all();
        $data = array(
            'phrases' => $publication
        );

        return view('phrase.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'author.required' => 'El campo autor es obligatorio.',
            'description.required' => 'El campo descripcion es obligatorio.',
            'title.required' => 'El campo titulo es obligatorio.'
        ];

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'author' => 'required',
            'description' => 'required'
        ], $messages);

        if ($validator->fails()) {
            return redirect('/phrase/create')
                ->withErrors($validator)
                ->withInput();
        }

        $phrase = $request->all();

        try {
            \DB::transaction(function () use ($phrase) {

                $row = new Phrase();
                $row->title = $phrase['title'];
                $row->author = $phrase['author'];
                $row->description = $phrase['description'];
                $row->save();
            });

        } catch (\ErrorException $e) {
            \Session::flash('add_errors', 'error');
            return redirect('/phrase/create')->with('add_errors', true);
        }

        return redirect('phrase/create')->with('status', 'Frase creada con éxito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $publication = Phrase::find($id);
        $data = array(
            'phrase' => $publication
        );

        return view('phrase.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'author.required' => 'El campo autor es obligatorio.',
            'description.required' => 'El campo descripcion es obligatorio.',
            'title.required' => 'El campo titulo es obligatorio.'
        ];

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'author' => 'required',
            'description' => 'required'
        ], $messages);

        if ($validator->fails()) {
            return redirect('/phrase/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput();
        }

        $phrase = $request->all();

        try {
            \DB::transaction(function () use ($phrase, $id) {

                $row = Phrase::find($id);
                $row->title = $phrase['title'];
                $row->author = $phrase['author'];
                $row->description = $phrase['description'];
                $row->save();
            });

        } catch (\ErrorException $e) {
            \Session::flash('add_errors', 'error');
            return redirect('/phrase/create')->with('add_errors', true);
        }

        return redirect('phrase/create')->with('status', 'Frase eliminada con éxito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $phrase = Phrase::find($id);


        try {

            $phrase->delete();

        } catch (\ErrorException $e) {
            \Session::flash('add_errors', 'error');
            return redirect('/phrase/create')->with('add_errors', true);
        }

        return redirect('phrase/create/')->with('status', 'Frase eliminada con éxito!');

    }
}
