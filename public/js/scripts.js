function upload(idImage)
{
    //Información del formulario
    var formData = new FormData($(".form-horizontal")[0]);

    var inputFile = document.getElementById(idImage);
    var file = inputFile.files[0];
    formData.append('documentation', file);
    formData.append('type', idImage);
    formData.append('_token', token_);

    if (file != undefined) {
        //hacemos la petición ajax
        $.ajax({
            url: '/image/upload',
            type: 'POST',
            // Form data
            //datos del formulario
            data: formData,
            //necesario para subir archivos via ajax
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json",
            //mientras enviamos el archivo
            beforeSend: function(){
                $("#upload_" + idImage).show();
                $("#upload_" + idImage).html('Validando el archivo... <i class="fa fa-refresh fa-spin fa-2x"></i>');
            },
            //una vez finalizado correctamente
            success: function(data){
                if (data.exito) {
                    $("#upload_" + idImage).html('El archivo se subio correctamente');
                    $("#" + idImage + "_name").val(data.file);
                    $("#" + idImage + "_campo").hide();
                    $("#" + idImage + "_descripcion").hide();
                } else {
                    $("#upload_" + idImage).html('El archivo no es valido');
                }
                $("#" + idImage).val('');
            },
            //si ha ocurrido un error
            error: function(){
                console.log("error");
                $("#upload_" + idImage).html('El archivo no es valido');
            }
        });
    }
}




function upload_image_publication(idImage, date)
{
    //Información del formulario
    var formData = new FormData($(".form-horizontal")[0]);

    var inputFile = document.getElementById(idImage);
    var file = inputFile.files[0];
    formData.append('documentation', file);
    formData.append('type', idImage);
    formData.append('_token', token_);
    formData.append('date', date);

    if (file != undefined) {
        //hacemos la petición ajax
        $.ajax({
            url: '/image/upload/publication',
            type: 'POST',
            // Form data
            //datos del formulario
            data: formData,
            //necesario para subir archivos via ajax
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json",
            //mientras enviamos el archivo
            beforeSend: function(){
                $("#upload_" + idImage).show();
                $("#upload_" + idImage).html('Validando el archivo... <i class="fa fa-refresh fa-spin fa-2x"></i>');
            },
            //una vez finalizado correctamente
            success: function(data){
                if (data.exito) {
                    $("#upload_" + idImage).html('El archivo se subio correctamente');
                    $("#" + idImage + "_name").val(data.file);
                    $("#" + idImage + "_campo").hide();
                    $("#" + idImage + "_descripcion").hide();
                } else {
                    $("#upload_" + idImage).html('El archivo no es valido');
                }
                $("#" + idImage).val('');
            },
            //si ha ocurrido un error
            error: function(){
                console.log("error");
                $("#upload_" + idImage).html('El archivo no es valido');
            }
        });
    }
}
