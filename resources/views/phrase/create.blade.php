@extends('layout')
@section('content')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Frase - Crear frase</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group"><p>&nbsp;</p></div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                    Por favor corrige los siguientes errores:<br>
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br/>
                    @endforeach
                </div>
            @endif

            @if (Session::has('status'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                    <strong>{{ Session::get('status') }}</strong>.
                </div>
            @endif

            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Información general
                            <small>Completa todos los campos del formulario</small>
                        </h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br/>
                        {!! Form::open(array('url' => 'phrase', 'id' => 'form','class'=>'form-horizontal')) !!}
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <input type="text" class="form-control has-feedback-left" required="required" name="title"
                                   placeholder="Titulo" value="">
                            <textarea class="form-control" rows="3"
                                      name="description"></textarea>
                            <input type="text" class="form-control has-feedback-left" required="required" name="author"
                                   placeholder="Autor" value="">
                            <span class="fa fa-pencil form-control-feedback left" aria-hidden="true"></span>
                        </div>


                        <div class="form-group">
                            <div class="col-md-9 col-sm-9 col-xs-12">&nbsp;</div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                <button type="reset" class="btn btn-primary">Cancelar</button>
                                <button type="submit" class="btn btn-success copyeditor">Guardar</button>
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
            @foreach($phrases as $phrase)
                <div class="col-md-4 col-xs-6">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{$phrase->title}} - {{$phrase->author}}</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                <div class="col-sm-8">
                                    <p>
                                        {{$phrase->description}}
                                    </p>
                                </div>
                                <div class="col-sm-4">

                                    {!! Form::open(array('route' => array('phrase.destroy', $phrase->id), 'method' => 'delete')) !!}
                                    <button type="submit" class="btn btn-danger"><span class="fa fa-trash"></span>
                                    </button>
                                    {!! Form::close() !!}


                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
@endsection

@section('scripts')
    @parent

    <!-- bootstrap-wysiwyg -->
    <script src="{{ asset('gentelella/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js') }}"></script>
    <script src="{{ asset('gentelella/jquery.hotkeys/jquery.hotkeys.js') }}"></script>
    <script src="{{ asset('gentelella/google-code-prettify/src/prettify.js') }}"></script>

    <script>
        $(document).ready(function () {

            $(".copyeditor").on("click", function () {
                var targetName = $("#editor").attr('data-target');
                $('#' + targetName).val($('#editor').html());
            });

            function initToolbarBootstrapBindings() {
                var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
                            'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
                            'Times New Roman', 'Verdana'
                        ],
                        fontTarget = $('[title=Font]').siblings('.dropdown-menu');
                $.each(fonts, function (idx, fontName) {
                    fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
                });
                $('a[title]').tooltip({
                    container: 'body'
                });
                $('.dropdown-menu input').click(function () {
                    return false;
                })
                        .change(function () {
                            $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
                        })
                        .keydown('esc', function () {
                            this.value = '';
                            $(this).change();
                        });

                $('[data-role=magic-overlay]').each(function () {
                    var overlay = $(this),
                            target = $(overlay.data('target'));
                    overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
                });

                if ("onwebkitspeechchange" in document.createElement("input")) {
                    var editorOffset = $('#editor').offset();

                    $('.voiceBtn').css('position', 'absolute').offset({
                        top: editorOffset.top,
                        left: editorOffset.left + $('#editor').innerWidth() - 35
                    });
                } else {
                    $('.voiceBtn').hide();
                }
            }

            function showErrorAlert(reason, detail) {
                var msg = '';
                if (reason === 'unsupported-file-type') {
                    msg = "El formato no es válido, solo se pueden subir imagenes";
                } else {
                    console.log("error uploading file", reason, detail);
                }
                $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>Error al subir la imagen</strong> ' + msg + ' </div>').prependTo('#alerts');
            }

            initToolbarBootstrapBindings();

            $('#editor').wysiwyg({
                fileUploadError: showErrorAlert
            });

            window.prettyPrint;
            prettyPrint();
        });

    </script>
    <!-- /bootstrap-wysiwyg -->

    <script type="text/javascript">
        var token_ = "{{ csrf_token() }}";
    </script>
    <script type="text/javascript" src="{{ asset('js/scripts.js') }}"></script>

@endsection
