<!DOCTYPE html>
<html>
<head>
    <title>Agencia CREO | Sustento de marcas</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta charset="utf-8">
    <meta name="author" content="Woorx">
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

    <!-- Favicons -->
    <link rel="shortcut icon" href="images/favicon.png">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">

    <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/style-responsive.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/vertical-rhythm.min.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/magnific-popup.css">


</head>
<body class="appear-animate">

<!-- Page Loader -->
<div class="page-loader">
    <div class="loader">Loading...</div>
</div>
<!-- End Page Loader -->

<!-- Page Wrap -->
<div class="page" id="top">

    <!-- Fullwidth Slider -->
    <div id="home" class="bg-dark relative">
        <div class="fullwidth-gallery">

            <!-- Slide Item -->
            <section class="home-section bg-scroll bg-dark-alfa-30" data-background="images/sliddy.jpg">
                <div class="js-height-full">
                </div>
            </section>
            <!-- End Slide Item -->
        </div>
        <!-- End Fullwidth Slider -->

        <!-- Header Content -->
        <div class="js-height-full fullwidth-galley-content">
            <!-- Hero Content -->
            <div class="home-content container">
                <div class="home-text">
                    <h1 class="nexa hidden-sm hidden-xs">CREO [EN]<br/> LO IMPOSIBLE.</h1>
                    <h1 class="nexa nexasmall hidden-lg hidden-md">CREO [EN] LO IMPOSIBLE.</h1>

                </div>
            </div>
            <!-- End Hero Content -->

            <!-- Scroll Down -->
            <div class="local-scroll">
                <a href="#about" class="scroll-down"><i class="fa fa-angle-down scroll-down-icon"></i></a>
            </div>
            <!-- End Scroll Down -->

        </div>
    </div>
    <!-- End Fullwidth Slider -->


    <!-- Navigation panel -->
    <nav class="main-nav dark transparent stick-fixed">
        <div class="full-wrapper relative clearfix">
            <!-- Logo ( * your text or image into link tag *) -->
            <div class="nav-logo-wrap local-scroll">
                <a href="#top" class="logo">
                    <img src="images/agencia-creo-logotop.png" alt=""/>
                </a>
            </div>
            <div class="mobile-nav">
                <i class="fa fa-bars"></i>
            </div>
            <!-- Main Menu -->
            <div class="inner-nav desktop-nav">
                <ul class="clearlist scroll-nav local-scroll">
                    <li class="active"><a href="#home">Inicio</a></li>
                    <li><a href="#about">Nosotros</a></li>
                    <li><a href="#services">Servicios</a></li>
                    <li><a href="#portfolio">Portafolio</a></li>
                    <li><a href="#contact">Contacto</a></li>

                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navigation panel -->


    <!-- About Section -->
    <section class="page-section pb-10" id="about">
        <div class="container relative">
            <h2 class="section-title font-alt align-center mb-10 mb-sm-40"
                style="color:#4AC19B;font-size: 28px;margin-bottom: 45px !important;">
                Acerca de CREO
            </h2>

            <div class="section-text">
                <div class="row">

                    <div class="col-md-2 col-md-offset-1 pullme hidden-xs hidden-sm">
                        <img src="images/logo-footer.png" alt="">
                    </div>

                    <div class="col-md-4 col-md-offset-1">
                        <blockquote>
                            <p>
                                Disfrutamos crear marcas y hacer publicidad. Nos gusta agregar valor.
                            </p>
                            <footer>
                                Agencia Creo
                            </footer>
                        </blockquote>
                    </div>

                    <div class="col-md-4 col-sm-12">
                        La imagen es percepción que se convierte en la identidad y con el tiempo en la reputación de una
                        marca. Una buena imagen generará una buena reputación.
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- End About Section -->

    <!-- Divider -->
    <hr class="mt-0 mb-0 "/>
    <!-- End Divider -->

    <!-- Services Section -->
    <section class="page-section" id="services">
        <div class="container relative">

            <h1 class="section-title font-alt mb-70 mb-sm-40" style="font-size: 2em !important;">
                Nuestros Servicios
            </h1>

            <div class="row">
                <div class="col-sm-12 text-center">
                    <img src="images/heartpencil.svg" width="100px" class="heartpencil" style="cursor:pointer;">
                </div>
            </div>

            <!-- Nav tabs -->
            <ul class="nav nav-tabs tpl-alt-tabs font-alt pt-30 pt-sm-0 pb-sm-0"
                style="height: 0;overflow: hidden; opacity: 0;">
                <li>
                    <a href="#service-branding" data-toggle="tab">

                        <div class="alt-tabs-icon">
                            <img src="images/heart.svg" class="serv-icon">
                        </div>
                        <span class="serv-text">Creación de marcas</span>
                    </a>
                </li>
                <li>
                    <a href="#service-graphic" data-toggle="tab">

                        <div class="alt-tabs-icon">
                            <img src="images/pencil.svg" class="serv-icon">
                        </div>

                        <span class="serv-text">Publicidad</span>
                    </a>
                </li>
            </ul>
            <!-- End Nav tabs -->

            <!-- Tab panes -->
            <div class="tab-content tpl-tabs-cont">

                <!-- Service Item -->
                <div class="tab-pane fade" id="service-branding">

                    <div class="section-text">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 mb-sm-50 mb-xs-30" style="text-align:center">
                                <ul class="serv-ul">
                                    {{--<span style="color:#33B39E;font-size: 3em;" class="icon-target"></span>--}}
                                    <li><h3 class="font-alt" style="font-size: 1.2em; margin-bottom: 3px;">Sustento de
                                            marca</h3>
                                        <p>Son como los cimientos de un edificio pero en tu marca.</p></li>
                                </ul>
                            </div>
                            <div class="col-md-6 col-sm-6 mb-sm-50 mb-xs-30" style="text-align:center">
                                <ul class="serv-ul">
                                    {{--<span style="color:#33B39E;font-size: 3em;" class="icon-tools"></span>--}}
                                    <li><h3 class="font-alt" style="font-size: 1.2em; margin-bottom: 3px;">Expresión
                                            Visual de Marca</h3>
                                        <p>Hacemos que tu marca tenga un buen look.</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- End Service Item -->

                <!-- Service Item -->
                <div class="tab-pane fade" id="service-graphic">

                    <div class="section-text">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 mb-sm-50 mb-xs-30" style="text-align:center">
                                <ul class="serv-ul">
                                    {{--<span style="font-size: 3em;color:#33B39E;" class="icon-megaphone"></span>--}}
                                    <li><h3 class="font-alt"
                                            style="text-align:center;font-size: 1.2em; margin-bottom: 3px;">campañas
                                            publicitarias</h3>
                                        <p>Le echamos creatividad para crear campañas exitosas.</p></li>
                                </ul>
                            </div>
                            <div class="col-md-4 col-sm-4 mb-sm-50 mb-xs-30" style="text-align:center">
                                <ul class="serv-ul">
                                    {{--<span style="font-size: 3em;color:#33B39E;" class="icon-video"></span>--}}
                                    <li><h3 class="font-alt"
                                            style="text-align:center;font-size: 1.2em; margin-bottom: 3px;">videos</h3>
                                        <p>Hacemos de una idea, una historia.</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-4 col-sm-4 mb-sm-50 mb-xs-30" style="text-align:center">
                                <ul class="serv-ul">
                                    {{--<span style="font-size: 3em;color:#33B39E;" class="icon-calendar"></span>--}}
                                    <li><h3 class="font-alt"
                                            style="text-align:center;font-size: 1.2em; margin-bottom: 3px;">contenidos
                                            para
                                            redes sociales</h3>
                                        <p>Mezclamos diseño con buenos contenidos.</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- End Service Item -->
        </div>
        <!-- End Tab panes -->

    </section>
    <!-- End Services Section -->


    <!-- Call Action Section -->
    <section class="page-section pt-0 pb-0 banner-section bg-dark"
             data-background="images/full-width-images/parallax-services.jpg">
        <div class="container relative">

            <div class="row">

                <div class="col-sm-6">

                    <div class="mt-140 mt-lg-80 mb-140 mb-lg-80">
                        <div class="banner-content">
                            <h3 class="banner-heading font-alt">CREEMOS en tu marca</h3>
                            <div class="banner-decription">
                                Todo gran proyecto comienza con una buena idea que se debe sustentar con análisis,
                                planeación y estrategia para contar una historia con pasión. Con una buena historia será
                                más
                                fácil expresar una imagen visualmente atractiva y memorable. Teniendo una buena historia
                                y
                                una imagen atractiva la marca podrá comunicarse y enamorar a su público objetivo.
                            </div>
                            <div class="local-scroll">
                                <a href="#contact" class="btn btn-mod btn-w btn-medium btn-round">Contáctanos</a>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-sm-6 banner-image wow fadeInUp" data-wow-duration="1.2s">
                    <img src="images/promo-1.png" alt=""/>
                </div>

            </div>

        </div>
    </section>
    <!-- End Call Action Section -->


    <!-- Portfolio Section -->
    <section class="page-section pb-0" id="portfolio">
        <div class="relative">

            <h1 class="section-title font-alt mb-70 mb-sm-40" style="font-size: 2em;">
                Portafolio
            </h1>

            <div class="container">
                <div class="row">

                </div>
            </div>

            <!-- Works Grid -->
            <ul class="works-grid work-grid-3 work-grid-gut clearfix font-alt hover-white hide-titles"
                id="work-grid">
                <!-- Work Item (Lightbox) -->
                @if(count($galleries) > 0)
                    @foreach($galleries AS $gallery)

                        <li class="work-item mix branding">
                            <a href="images/{{ $gallery->file_big }}" class="work-lightbox-link mfp-image">
                                <div class="work-img">
                                    <img src="images/{{ $gallery->file_small }}" alt="Work"/>
                                </div>
                                <div class="work-intro">
                                    <h3 class="work-title">{{ $gallery->title }}</h3>
                                    <div class="work-descr">
                                        {{ $gallery->description }}
                                    </div>
                                </div>
                            </a>
                            @if($gallery->file_big2)
                                <a href="images/{{ $gallery->file_big2 }}" class="work-lightbox-link mfp-image"
                                   style="display: none;">
                                </a>
                            @endif
                            @if($gallery->file_big3)
                                <a href="images/{{ $gallery->file_big3 }}" class="work-lightbox-link mfp-image"
                                   style="display: none;">
                                </a>
                            @endif
                        </li>

                        <!-- End Work Item -->
                    @endforeach
                @endif
            </ul>

        </div>
    </section>
    <!-- End Portfolio Section -->

    <!-- Testimonials Section -->
    <section class="page-section bg-dark bg-dark-alfa-90 fullwidth-slider"
             data-background="images/full-width-images/parallax-testimoniales.jpg">

        <!-- Slide Item -->
        @if($phrases->count())
            @foreach($phrases as $phrase)
                <div>
                    <div class="container relative">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 align-center">
                                <!-- Section Icon -->
                                <div class="section-icon">
                                    <span style="color: #4AC19B;" class="icon-quote"></span>
                                </div>
                                <!-- Section Title --><h3 class="small-title font-alt">{{ $phrase->title }}</h3>
                                <blockquote class="testimonial white">
                                    <p>
                                        {{ $phrase->description }}
                                    </p>
                                    <footer class="testimonial-author">
                                        {{ $phrase->author }}
                                    </footer>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <div>
                <div class="container relative">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 align-center">
                            <!-- Section Icon -->
                            <div class="section-icon">
                                <span style="color: #4AC19B;" class="icon-quote"></span>
                            </div>
                            <!-- Section Title --><h3 class="small-title font-alt">Agencia Creo</h3>
                            <blockquote class="testimonial white">
                                <p>
                                    CREO [EN] LO IMPOSIBLE.
                                </p>
                                <footer class="testimonial-author">
                                    Agencia Creo
                                </footer>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>
    @endif
    <!-- End Slide Item -->

    </section>
    <!-- End Testimonials Section -->

    <!-- Contact Section -->
    <section class="page-section" id="contact">
        <div class="container relative">

            <h2 class="section-title font-alt mb-70 mb-sm-40">
                Contacto
            </h2>

            <div class="row mb-60 mb-xs-40">

                <div class="col-md-8 col-md-offset-2">
                    <div class="row">

                        <!-- Phone -->
                        <div class="col-sm-6 col-lg-4 pt-20 pb-20 pb-xs-0">
                            <div class="contact-item">
                                <div class="ci-icon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <div class="ci-title font-alt">
                                    Llámanos
                                </div>
                                <div class="ci-text">
                                    (442) 23 85 18 74
                                </div>
                            </div>
                        </div>
                        <!-- End Phone -->

                        <!-- Address -->
                        <div class="col-sm-6 col-lg-4 pt-20 pb-20 pb-xs-0">
                            <div class="contact-item">
                                <div class="ci-icon">
                                    <i class="fa fa-map-marker"></i>
                                </div>
                                <div class="ci-title font-alt">
                                    Ubícanos
                                </div>
                                <div class="ci-text">
                                    Universidad #222 int204 Col. San Javier
                                </div>
                            </div>
                        </div>
                        <!-- End Address -->

                        <!-- Email -->
                        <div class="col-sm-6 col-lg-4 pt-20 pb-20 pb-xs-0">
                            <div class="contact-item">
                                <div class="ci-icon">
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <div class="ci-title font-alt">
                                    Email
                                </div>
                                <div class="ci-text">
                                    <a href="mailto:hola@agenciacreo.com">hola@agenciacreo.com</a>
                                </div>
                            </div>
                        </div>
                        <!-- End Email -->

                    </div>
                </div>

            </div>

            <!-- Contact Form -->
            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    <form class="form contact-form" id="contact_form">
                        <div class="clearfix">

                            <div class="cf-left-col">

                                <!-- Name -->
                                <div class="form-group">
                                    <input type="text" name="name" id="name" class="input-md round form-control"
                                           placeholder="Nombre" pattern=".{3,100}" required>
                                </div>

                                <!-- Email -->
                                <div class="form-group">
                                    <input type="email" name="email" id="email" class="input-md round form-control"
                                           placeholder="Email" pattern=".{5,100}" required>
                                </div>

                            </div>

                            <div class="cf-right-col">

                                <!-- Message -->
                                <div class="form-group">
                                <textarea name="message" id="message" class="input-md round form-control"
                                          style="height: 84px;" placeholder="Mensaje"></textarea>
                                </div>

                            </div>

                        </div>

                        <div class="clearfix">

                            <div class="cf-left-col">

                                <!-- Inform Tip -->
                                <div class="form-tip pt-20">
                                    <i class="fa fa-info-circle"></i> Todos los campos son requeridos
                                </div>

                            </div>

                            <div class="cf-right-col">

                                <!-- Send Button -->
                                <div class="align-right pt-10">
                                    <button class="submit_btn btn btn-mod btn-medium btn-round" id="submit_btn">Enviar
                                        mensaje
                                    </button>
                                </div>

                            </div>

                        </div>


                        <div id="result"></div>
                    </form>

                </div>
            </div>
            <!-- End Contact Form -->

        </div>
    </section>
    <!-- End Contact Section -->


    <!-- Google Map -->
    <div class="google-map">

        <div data-address="Avenida Universidad 222, Colonia San Javier" id="map-canvas"></div>

        <div class="map-section">

            <div class="map-toggle">
                <div class="mt-icon">
                    <i class="fa fa-map-marker"></i>
                </div>
                <div class="mt-text font-alt">
                    <div class="mt-open">Ver mapa <i class="fa fa-angle-down"></i></div>
                    <div class="mt-close">Cerrar mapa <i class="fa fa-angle-up"></i></div>
                </div>
            </div>

        </div>

    </div>
    <!-- End Google Map -->


    <!-- Foter -->
    <footer class="page-section bg-gray-lighter footer pb-60">
        <div class="container">

            <!-- Footer Logo -->
            <div class="local-scroll mb-30 wow fadeInUp" data-wow-duration="1.2s">
                <a href="#top"><img src="images/logo-footer.png" width="130px" alt=""/></a>
            </div>
            <!-- End Footer Logo -->

            <!-- Social Links -->
            <div class="footer-social-links mb-110 mb-xs-60">
                <a href="http://www.facebook.com/creo.agencia" title="Facebook" target="_blank"><i
                            class="fa fa-facebook"></i></a>
                {{--<a href="#" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a>--}}
                <a href="https://www.behance.net/agenciacreo" title="Behance" target="_blank"><i
                            class="fa fa-behance"></i></a>
                <a href="https://www.instagram.com/agenciacreo/" title="Instagram+" target="_blank"><i
                            class="fa fa-instagram"></i></a>
                {{--<a href="#" title="Pinterest" target="_blank"><i class="fa fa-pinterest"></i></a>--}}
            </div>
            <!-- End Social Links -->

            <!-- Footer Text -->
            <div class="footer-text">

                <!-- Copyright -->
                <div class="footer-copy font-alt">
                    <a href="#" target="_blank">&copy; Woorx & Agencia CREO 2016</a>.
                </div>
                <!-- End Copyright -->

                <div class="footer-made">
                    Hecho creyendo en el compañerismo.
                </div>

            </div>
            <!-- End Footer Text -->

        </div>


        <!-- Top Link -->
        <div class="local-scroll">
            <a href="#top" class="link-to-top"><i class="fa fa-caret-up"></i></a>
        </div>
        <!-- End Top Link -->

    </footer>
    <!-- End Foter -->


</div>
<!-- End Page Wrap -->


<!-- JS -->
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/SmoothScroll.js"></script>
<script type="text/javascript" src="js/jquery.scrollTo.min.js"></script>
<script type="text/javascript" src="js/jquery.localScroll.min.js"></script>
<script type="text/javascript" src="js/jquery.viewport.mini.js"></script>
<script type="text/javascript" src="js/jquery.countTo.js"></script>
<script type="text/javascript" src="js/jquery.appear.js"></script>
<script type="text/javascript" src="js/jquery.sticky.js"></script>
<script type="text/javascript" src="js/jquery.parallax-1.1.3.js"></script>
<script type="text/javascript" src="js/jquery.fitvids.js"></script>
<script type="text/javascript" src="js/owl.carousel.min.js"></script>
<script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="js/jquery.magnific-popup.js"></script>
<script type="text/javascript"
        src="http://maps.google.com/maps/api/js?key=AIzaSyDPdQ1gq9KmPjCFXdJnOZlthdDPLAPajR4&amp;sensor=false&amp;language=en"></script>
<script type="text/javascript" src="js/gmap3.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="js/jquery.simple-text-rotator.min.js"></script>
<script type="text/javascript" src="js/all.js"></script>
<script type="text/javascript" src="js/contact-form.js"></script>
<script type="text/javascript" src="js/jquery.ajaxchimp.min.js"></script>
<!--[if lt IE 10]>
<script type="text/javascript" src="js/placeholder.js"></script><![endif]-->

</body>
</html>
