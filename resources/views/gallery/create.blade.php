@extends('layout')
@section('content')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Galería de imagenes - Nueva imagen</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group"><p>&nbsp;</p></div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            @if (count($errors) > 0)
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                    Por favor corrige los siguientes errores:<br>
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br/>
                    @endforeach
                </div>
            @endif

            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Información general
                            <small>Completa todos los campos del formulario</small>
                        </h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content row">
                        <form class="form-horizontal form-label-left input_mask" method="POST" action="/gallery"
                              enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="col-sm-6  form-group has-feedback">
                                    <input type="text" class=" form-control has-feedback-left" name="title"
                                           placeholder="Título">
                                    <span class="fa fa-pencil form-control-feedback left" aria-hidden="true"></span>
                                </div>
                            <div class="col-sm-6 form-group has-feedback">
                                <label class="col-md-2 control-label">Descripción</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" name="description" rows="5"></textarea>
                                </div>
                            </div>


                            <div class="col-sm-6 form-group has-feedback">
                                <div class="col-md-10">
                                    <div class="input-group" id="small_campo">
                                        <span class="input-group-addon"><span class="fa fa-image"></span> Imagen pequeña</span>
                                        <input type="file" name="small" id="small" class="form-control"
                                               onChange="upload('small')"/>
                                        <input type="hidden" name="image_small" id="small_name" value=""/>
                                    </div>
                                    <span class="help-block" id="small_descripcion">
                                      Solo se permiten archivos tipo JPG y PNG  con un tamaño máximo de 2mb.<br/>650 x 418 pixeles
                                    </span>
                                    <span id="upload_small" class="help-block"
                                          style="display:none; color:#1caf9a !important">Validando el archivo... <i
                                                class="fa fa-refresh fa-spin fa-2x"></i></span>
                                </div>
                            </div>

                            <div class="col-sm-6 form-group has-feedback">
                                <div class="col-md-10">
                                    <div class="input-group" id="big_campo">
                                        <span class="input-group-addon"><span class="fa fa-image"></span> Imagen grande 1</span>
                                        <input type="file" name="big" id="big" class="form-control"
                                               onChange="upload('big')"/>
                                        <input type="hidden" name="image_big" id="big_name" value=""/>
                                    </div>
                                    <span class="help-block" id="big_descripcion">
                          Solo se permiten archivos tipo JPG y PNG  con un tamaño máximo de 2mb.<br/>1140 x 642 pixeles
                        </span>
                                    <span id="upload_big" class="help-block"
                                          style="display:none; color:#1caf9a !important">Validando el archivo... <i
                                                class="fa fa-refresh fa-spin fa-2x"></i></span>
                                </div>
                            </div>

                            <div class="col-sm-6 form-group has-feedback">
                                <div class="col-md-10">
                                    <div class="input-group" id="big2_campo">
                                        <span class="input-group-addon"><span class="fa fa-image"></span> Imagen grande 2</span>
                                        <input type="file" name="big2" id="big2" class="form-control"
                                               onChange="upload('big2')"/>
                                        <input type="hidden" name="image_big2" id="big2_name" value=""/>
                                    </div>
                                    <span class="help-block" id="big2_descripcion">
                          Solo se permiten archivos tipo JPG y PNG  con un tamaño máximo de 2mb.<br/>1140 x 642 pixeles
                        </span>
                                    <span id="upload_big2" class="help-block"
                                          style="display:none; color:#1caf9a !important">Validando el archivo... <i
                                                class="fa fa-refresh fa-spin fa-2x"></i></span>
                                </div>
                            </div>

                            <div class="col-sm-6 form-group has-feedback">
                                <div class="col-md-10">
                                    <div class="input-group" id="big3_campo">
                                        <span class="input-group-addon"><span class="fa fa-image"></span> Imagen grande 3</span>
                                        <input type="file" name="big3" id="big3" class="form-control"
                                               onChange="upload('big3')"/>
                                        <input type="hidden" name="image_big3" id="big3_name" value=""/>
                                    </div>
                                    <span class="help-block" id="big3_descripcion">
                          Solo se permiten archivos tipo JPG y PNG  con un tamaño máximo de 2mb.<br/>1140 x 642 pixeles
                        </span>
                                    <span id="upload_big3" class="help-block"
                                          style="display:none; color:#1caf9a !important">Validando el archivo... <i
                                                class="fa fa-refresh fa-spin fa-2x"></i></span>
                                </div>
                            </div>




                            <div class="form-group">
                                <div class="col-md-9 col-sm-9 col-xs-12">&nbsp;</div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                    <button type="reset" class="btn btn-primary">Cancelar</button>
                                    <button type="submit" class="btn btn-success copyeditor">Guardar</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script type="text/javascript">
        var token_ = "{{ csrf_token() }}";
    </script>
    <script type="text/javascript" src="{{ asset('js/scripts.js') }}"></script>

@endsection
