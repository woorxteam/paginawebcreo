@extends('layout')
@section('content')
<div class="">
    <div class="page-title">
      <div class="title_left">
        <h3> Galería de imagenes </h3>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <a href="/gallery/create">
                <button class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-plus"></i> Nueva imagen</button>
            </a>
          </div>
        </div>
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Listado <small> de imagenes </small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <div class="row">
                @if(count($galleries) > 0)
                    @foreach($galleries AS $gallery)
                        <div class="col-md-3">
                          <div class="thumbnail">
                            <div class="image view view-first">
                              <img style="width: 100%; display: block;" src="/images/{{ $gallery->file_small }}" alt="image" />
                              <div class="mask">
                                <p>Acciones</p>
                                <div class="tools tools-bottom">
                                  <button type="button" class="btn btn-danger deleteBtn" rel="{{ $gallery->id }}" data-toggle="modal" data-target="#myModal"><i class="fa fa-trash"></i></button>
                                </div>
                              </div>
                            </div>
                            <div class="caption">
                              <p>{{ $gallery->title }}</p>
                            </div>
                          </div>
                        </div>
                    @endforeach
                @endif
            </div>
          </div>
        </div>
      </div>
    </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><span class="fa fa-times"></span> Advertencia</h4>
        </div>
        <div class="modal-body">
          <p>¿Está seguro que desea eliminar esta galería?</p>
        </div>
        <div class="modal-footer">
          <div class="col-md-12 pull-left">
                {!! Form::open(array('url' => 'gallery', 'id'  => 'deleteForm')) !!}
                  {!! Form::hidden('_method', 'DELETE') !!}
                  {!! Form::submit('Eliminar', array('class' => 'btn btn-danger btn-lg pull-left')) !!}
                  <button type="button" class="btn btn-default btn-lg" data-dismiss="modal">Cancelar</button>
                {!! Form::close() !!}

          </div>
        </div>
      </div>

    </div>
</div>
@endsection

@section('scripts')
      @parent

      <script type="text/javascript">
        $(document).ready(function() {
          $( ".deleteBtn" ).click(function() {
              var id = $(this).attr("rel");
              $('#deleteForm').attr('action', 'gallery/' + id);
          });
        });
      </script>
@endsection
