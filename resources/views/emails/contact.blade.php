<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Tours Ejecutivos</title>
<meta name="Concurso Ciinova" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Passion+One" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="{{ asset('assets_mail/css/bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets_mail/css/style.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets_mail/css/animate.css') }}">
</head>

<body itemscope itemtype="http://schema.org/EmailMessage">

  <div class="container-fluid nomargin nopadding imagen2 centered">
    <div class="row parrafomaslogo">
      <div class="col-md-6 col-center">

         <p style="margin-top:20px;font-weight:bold;width:500px;margin:auto;padding-top:10%;">
           Contacto desde Tours ejecutivos<br />
           <b>Nombre:</b> {{ $name }}.<br />
           <b>E-mail:</b> {{ $email }}.<br />
           <b>Teléfono:</b> {{ $phone }}.<br />
           <b>Area que desea comunicarse:</b> {{ $referrer }}<br />
           <b>Mensaje:</b> {{ $message_content }}
         </p>
      </div>
      <div class="col-md-6 col-center">

      </div>
    </div>
  </div>

</body>
</html>
